package ru.tsc.denisturovsky.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;

import java.util.Date;

public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

    @NotNull
    ProjectDTO create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    ProjectDTO create(
            @NotNull String userId,
            @NotNull String name
    ) throws Exception;

    @NotNull
    ProjectDTO create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @NotNull Date dateBegin,
            @NotNull Date dateEnd
    ) throws Exception;

}
