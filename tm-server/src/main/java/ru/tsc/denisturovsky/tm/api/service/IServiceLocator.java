package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.service.dto.*;

public interface IServiceLocator {

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IServerLoggerService getLoggerService();

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    IProjectTaskDTOService getProjectTaskService();

    @NotNull
    IServerPropertyService getPropertyService();

    @NotNull
    ISessionDTOService getSessionService();

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IUserDTOService getUserService();

}
