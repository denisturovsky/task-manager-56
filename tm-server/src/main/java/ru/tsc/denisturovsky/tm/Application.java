package ru.tsc.denisturovsky.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.denisturovsky.tm.component.ServerBootstrap;
import ru.tsc.denisturovsky.tm.configuration.ServerConfiguration;

public final class Application {

    public static void main(@Nullable String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final ServerBootstrap serverBootstrap = context.getBean(ServerBootstrap.class);
        serverBootstrap.run();
    }

}
