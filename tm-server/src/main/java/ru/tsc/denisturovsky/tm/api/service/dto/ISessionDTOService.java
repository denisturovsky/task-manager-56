package ru.tsc.denisturovsky.tm.api.service.dto;

import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {

}
